# WoH Article Parser

### Requirements

- python3-pip
- python3-dev
- libxml2-dev
- libxslt1-dev
- libjpeg-dev
- zlib1g-dev
- libpng-dev

```bash
$ curl https://raw.githubusercontent.com/codelucas/newspaper/master/download_corpora.py | python
```
