FROM python:3.6-alpine

RUN apk add curl libxml2-dev libxslt-dev libjpeg-turbo-dev zlib-dev libpng-dev build-base

EXPOSE 8080

RUN mkdir -p /app
WORKDIR /app

COPY requirements.txt .
COPY article.py .
RUN pip install --no-cache-dir -r requirements.txt
RUN curl https://raw.githubusercontent.com/codelucas/newspaper/master/download_corpora.py | python

ENTRYPOINT ["python3", "article.py"]
