#!/usr/bin/env python3

from dotenv import load_dotenv
import os
import json
from newspaper.article import ArticleDownloadState, Article
from tornado.web import Application, RequestHandler
from tornado.ioloop import IOLoop
import sentry_sdk
from sentry_sdk.integrations.tornado import TornadoIntegration


class ArticleHandler(RequestHandler):
    def post(self):
        request = json.loads(self.request.body.decode('utf-8'))

        article = Article(request['url'], language='ru')
        article.set_html(request['body'])
        article.download_state = ArticleDownloadState.SUCCESS
        article.parse()

        self.finish(json.dumps({
            'title': article.title,
            'image': article.top_image,
            'authors': article.authors,
            'text': article.text,
            'html': article.article_html,
            'date': str(article.publish_date),
            'movies': article.movies
        }))


if __name__ == '__main__':
    load_dotenv()

    if os.getenv('SENTRY_DSN'):
        sentry_sdk.init(
            dsn=os.getenv('SENTRY_DSN'),
            integrations=[TornadoIntegration()]
        )

    app = Application([
        (r'/article', ArticleHandler),
    ])
    app.listen(os.getenv('PORT', 8080))
    IOLoop.current().start()
